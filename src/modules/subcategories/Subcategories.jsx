import React, { useEffect, useState, } from "react"
import { 
    List, Avatar, Divider, Button,
    Skeleton, Breadcrumb, Popover,
    Spin,
} from 'antd';
import SubcategoryCreate from "./SubcategoryCreate"
import notification from "lib/helpers/notification"
import { 
  DELETE_CATEGORY as deleteCategoryUrl,
  FETCH_SUBCATEGORIES_BY_CATEGORY as fetchSubcatByCatUrl,
} from "lib/network/endpoints"
import { DELETE, GET } from "lib/network/requests"
import _ from "underscore"
import { withRouter } from "react-router";

const Categories = ({match})=>{
    const [ visiblePopover, setVisiblePopover ] = useState(null)
    const [ subcategories, setSubcategories ] = useState([])
    const [ categoriesBeingDeleted, setCategoriesBeingDeleted ] = useState([])
    
    useEffect(()=>{
      getSubcategories()
    },[])

    const getSubcategories = ()=>{
      const categoryId = match?.params?.categoryId
      GET({url:fetchSubcatByCatUrl, data:{categoryId}},(err,res)=>{
        if(err){
          return notification.error(err?.message)
        }
        setSubcategories(_.clone(res?.subCategories))
      })
    }

    const onDeleteCategory=(categoryId)=>{
        setCategoriesBeingDeleted(_.union(categoriesBeingDeleted,[categoryId]))
        setVisiblePopover(null)
    
        DELETE({ url: deleteCategoryUrl, data:{categoryId} },(err,res)=>{
          if(err){
            return notification.error(err?.message)
          }
          getSubcategories()
          setCategoriesBeingDeleted(_.without(categoriesBeingDeleted,categoryId))
          return notification.success(res?.message)
        })
      }

    const getContent=(categoryId)=>{
        return(
          <Button onClick={()=>onDeleteCategory(categoryId)} danger>Da</Button>
        )
      }

      const handleVisibleChange = categoryId => {
        if(categoryId === visiblePopover){
          categoryId = null
        }
    
        setVisiblePopover(categoryId)
      };

      return(
        <>
            <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item href="/admin/categorii/">Categorii</Breadcrumb.Item>
              <Breadcrumb.Item>Categorie </Breadcrumb.Item>
              <Breadcrumb.Item>Subcategorii </Breadcrumb.Item>

            </Breadcrumb>
            <Divider/>
            <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
            
            <SubcategoryCreate 
              getSubcategories={getSubcategories}
            />
            <Divider/>

            <List
                loading={false}
                itemLayout="horizontal"
                // loadMore={loadMore}
                dataSource={subcategories}
                renderItem={(item,idx) => (
                <List.Item
                    actions={[
                    <Button key="1">Editeaza</Button>, 
                    <Popover 
                        key="2"
                          content={getContent(item?._id)} 
                          visible={visiblePopover === item?._id}
                          onVisibleChange={()=>handleVisibleChange(item?._id)}
                          title="Ești sigur?" 
                          trigger="click"
                        >
                          <Button 
                            disabled={_.includes(categoriesBeingDeleted, item?._id)} 
                            type="danger" 
                            key="delete"
                          >
                            {
                              _.includes(categoriesBeingDeleted, item?._id)
                              ? <Spin/>
                              : "Șterge"
                            }
                          </Button>
                        </Popover>
                ]}
                >
                    <Skeleton avatar title={false} loading={item?.loading} active>
                    <List.Item.Meta
                        avatar={
                        <Avatar>{idx+1}</Avatar>
                        }
                        title={<a href="https://ant.design">{item?.name}</a>}
                        description="Ant Design, a design language for background applications, is refined by Ant UED Team"
                    />
                    <div>-</div>
                    </Skeleton>
                </List.Item>
                )}
            />
            </div>
        </>
    )
}

export default withRouter(Categories)