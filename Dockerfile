FROM node:14 as builder 

WORKDIR /opt/frontend

# Installing dependencies
COPY package.json .
COPY yarn.lock .

RUN yarn
COPY . .

RUN yarn build

FROM nginx
EXPOSE 3000
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /opt/frontend/build /usr/share/nginx/html