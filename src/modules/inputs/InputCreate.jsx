import React, { useState } from 'react'
import { Drawer, Button } from 'antd'
import { Form, Input, Spin, Select } from 'antd'
import { POST } from "lib/network/requests"
import { CREATE_TEMP_INPUT as createUrl } from "lib/network/endpoints"
import notification from "lib/helpers/notification"
import { withRouter } from "react-router"
import _ from "underscore"

const { Option } = Select

const inputTypes=[
  {
    label:"Text",
    value:"string",
  },
  {
    label:"Text lung",
    value:"text",
  },
  {
    label:"Numar",
    value:"number"
  },
  {
    label:"An",
    value:"year",
  },
  {
    label:"Pret",
    value:"price",
  },
  {
    label:"Optiuni",
    value:"select",
  },
]

const Create= ({ match, getInputs}) => {

  const [visible, setVisible] = useState(false)
  const [isCreating, setIsCreating] = useState(false)
  const [inputType, setInputType] = useState(null)

  const showDrawer = () => {
    setVisible(true)
  }

  const closeDrawer = () => {
    setVisible(false)
  }

  const onFinish = (data) => {
    setIsCreating(true)
    POST({ url:createUrl, data },(err,res)=>{
        setIsCreating(false)
        if(err){
            return notification.error(err?.message)
        }
        getInputs()
        setVisible(false)
        return notification.success(res?.message)
    })
  }

  const onSelectInputType=(inputType)=>setInputType(inputType)

  const onFinishFailed = (errorInfo) => {
  }

  return (
    <>
      <Button shape="circle" type="primary" onClick={showDrawer}>
        +
      </Button>

      <Drawer
        width={500}
        title="Creaza camp"
        placement="right"
        closable={false}
        onClose={closeDrawer}
        visible={visible}
      >
       <Form
      name="basic"
      initialValues={{}}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Form.Item
        name="name"
        rules={[{ required: true, message: 'Completeaza nume!' }]}
      >
        <Input
            placeholder="Nume"
        />
      </Form.Item>

      <Form.Item name="dataType" rules={[{ required: true }]}>
        <Select
          placeholder="Tipul"
          onChange={onSelectInputType}
          allowClear
        >
          {
            _.map(inputTypes,i=>(
              <Option value={i?.value}>{i?.label}</Option>

            ))
          }
        </Select>
      </Form.Item>

      {
        inputType==="select"
        &&<Form.Item  name={["meta","select","options"]} rules={[{ required: true }]}>
          <Select mode="tags" style={{ width: '100%' }} placeholder="Tags Mode"/>
        </Form.Item>
      }

      <Form.Item>
        <Button type="primary" block htmlType="submit">
          {
              isCreating
              ? <Spin/>
              : "Creaza"
          }
        </Button>
      </Form.Item>
    </Form>
      </Drawer>
    </>
  )
}

export default withRouter(Create)