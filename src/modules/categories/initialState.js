
const initialState = {
    categories: [],
    categoriesTotal: 0,
    categoriesLoading: false,
    categoriesFilters: {},
    categoriesOptions: {},
    categorie: {},
    categorieLoading: false,
}

export default initialState