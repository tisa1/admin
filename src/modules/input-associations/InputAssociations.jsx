import React, { useEffect, useState, useCallback } from "react"
import { 
    List, Avatar, Divider, Button,
    Skeleton, Breadcrumb, Popover,
    Spin,
} from 'antd';
import InputAssocCreate from "./InputAssociationCreate"
import notification from "lib/helpers/notification"
import { 
  DELETE_TEMP_INP_ASSOCIATION as deleteUrl,
  FETCH_TEMP_INP_ASSOCIATIONS_BY_TEMPLATE as fetchUrl,
} from "lib/network/endpoints"
import { DELETE, GET } from "lib/network/requests"
import _ from "underscore"
import { withRouter } from "react-router";

const Categories = ({ match })=>{
    const templateId = match?.params?.templateId
    const [ visiblePopover, setVisiblePopover ] = useState(null)
    const [ items, setItems ] = useState([])
    const [ itemsBeingDeleted, setItemsBeingDeleted ] = useState([])
    
    const getItems = useCallback(()=>{
      GET({ url:fetchUrl, data:{ templateId } },(err,res)=>{
        if(err){
          return notification.error(err?.message)
        }
        setItems(_.clone(res?.tempInpAssociations))
      })
    },[ templateId ])

    useEffect(()=>{
      getItems()
    },[ getItems ])

    const onDeleteItem=(tempInpAssocId)=>{
      setItemsBeingDeleted(_.union(itemsBeingDeleted,[tempInpAssocId]))
      setVisiblePopover(null)
  
      DELETE({ url: deleteUrl, data:{ tempInpAssocId } },(err,res)=>{
        setItemsBeingDeleted(_.without(itemsBeingDeleted,tempInpAssocId))
        if(err){
          return notification.error(err?.message)
        }
        getItems()
        return notification.success(res?.message)
      })
    }

    const getContent=(itemId)=>{
        return(
          <Button onClick={()=>onDeleteItem(itemId)} danger>Da</Button>
        )
      }

      const handleVisibleChange = itemId => {
        if(itemId === visiblePopover){
          itemId = null
        }
    
        setVisiblePopover(itemId)
      }
      
      return(
        <>
            <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item href="/admin/templateuri"> Template-uri </Breadcrumb.Item>
              <Breadcrumb.Item> {templateId} </Breadcrumb.Item>
              <Breadcrumb.Item> Campuri asociate </Breadcrumb.Item>
            </Breadcrumb>
            <Divider/>
            <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
            
            <InputAssocCreate 
              getInputAssociations={getItems}
            />

            <Divider/>

            <List
                loading={false}
                itemLayout="horizontal"
                dataSource={items}
                renderItem={(item,idx) => (
                <List.Item
                    actions={[
                    <Button key="1">Editeaza</Button>, 
                    <Popover 
                        key="2"
                          content={getContent(item?._id)} 
                          visible={visiblePopover === item?._id}
                          onVisibleChange={()=>handleVisibleChange(item?._id)}
                          title="Ești sigur?" 
                          trigger="click"
                        >
                          <Button 
                            disabled={_.includes(itemsBeingDeleted, item?._id)} 
                            type="danger" 
                            key="delete"
                          >
                            {
                              _.includes(itemsBeingDeleted, item?._id)
                              ? <Spin/>
                              : "Șterge"
                            }
                          </Button>
                        </Popover>
                ]}
                >
                    <Skeleton avatar title={false} loading={item?.loading} active>
                    <List.Item.Meta
                        avatar={
                        <Avatar>{idx+1}</Avatar>
                        }
                        title={<a href={`/admin/templateuri/${item?._id}`}>{item?.tempInput?.name}</a>}
                        description={item?.subcategory?.name}
                    />

                    <div>-</div>
                    </Skeleton>
                </List.Item>
                )}
            />
            </div>
        </>
    )
}

export default withRouter(Categories)