import React, { useEffect, useState, } from "react"
import { 
    List, Avatar, Divider, Button,
    Skeleton, Breadcrumb, Popover,
    Spin,
} from 'antd';
import { useSelector, useDispatch } from "react-redux";
import { getCategories } from "./actions/categories"
import CategoryCreate from "./CategoryCreate"
import notification from "lib/helpers/notification"
import { DELETE_CATEGORY as deleteCategoryUrl } from "lib/network/endpoints"
import { DELETE } from "lib/network/requests"
import _ from "underscore"

const Categories = ()=>{
    const dispatch = useDispatch();
    const [ visiblePopover, setVisiblePopover ] = useState(null)
    const [ categoriesBeingDeleted, setCategoriesBeingDeleted ] = useState([])

    useEffect(()=>{
        dispatch(getCategories())
    },[dispatch])
    
    const categories = useSelector((state) => state.categories)

    const onDeleteCategory=(categoryId)=>{
        setCategoriesBeingDeleted(_.union(categoriesBeingDeleted,[categoryId]))
        setVisiblePopover(null)
    
        DELETE({ url: deleteCategoryUrl, data:{categoryId} },(err,res)=>{
          setCategoriesBeingDeleted(_.without(categoriesBeingDeleted,categoryId))
          if(err){
            return notification.error(err?.message)
          }
          dispatch(getCategories())
          return notification.success(res?.message)
        })
      }

    const getContent=(categoryId)=>{
        return(
          <Button onClick={()=>onDeleteCategory(categoryId)} danger>Da</Button>
        )
      }

      const handleVisibleChange = categoryId => {
        if(categoryId === visiblePopover){
          categoryId = null
        }
    
        setVisiblePopover(categoryId)
      };

      return(
        <>
            <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item href="/admin/categorii">Categorii</Breadcrumb.Item>
              <Breadcrumb.Item>Categorie </Breadcrumb.Item>
            </Breadcrumb>
            <Divider/>
            <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
            
            <CategoryCreate/>
            <Divider/>

            <List
                loading={false}
                itemLayout="horizontal"
                // loadMore={loadMore}
                dataSource={categories}
                renderItem={(item,idx) => (
                <List.Item
                    actions={[
                    <Button key="1">Editeaza</Button>, 
                    <Popover 
                        key="2"
                          content={getContent(item?._id)} 
                          visible={visiblePopover === item?._id}
                          onVisibleChange={()=>handleVisibleChange(item?._id)}
                          title="Ești sigur?" 
                          trigger="click"
                        >
                          <Button 
                            disabled={_.includes(categoriesBeingDeleted, item?._id)} 
                            type="danger" 
                            key="delete"
                          >
                            {
                              _.includes(categoriesBeingDeleted, item?._id)
                              ? <Spin/>
                              : "Șterge"
                            }
                          </Button>
                        </Popover>
                ]}
                >
                    <Skeleton avatar title={false} loading={item?.loading} active>
                    <List.Item.Meta
                        avatar={
                        <Avatar>{idx+1}</Avatar>
                        }
                        title={<a href={`/admin/categorii/${item?._id}`}>{item?.name}</a>}
                        description="Ant Design, a design language for background applications, is refined by Ant UED Team"
                    />
                    <div>-</div>
                    </Skeleton>
                </List.Item>
                )}
            />
            </div>
        </>
    )
}

export default Categories