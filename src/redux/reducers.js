import { combineReducers } from "redux"
import _ from "underscore"
import * as appReducers from "./reducers/app"
import * as categoriesReducer from "modules/categories/reducers"

// Combining reducers
_.extend(
  appReducers,
  categoriesReducer,
)

export default combineReducers(appReducers)
