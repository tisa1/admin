
import {
  GET
} from "lib/network/requests"
import {
  FETCH_CATEGORIES as getCategoriesUrl,
} from "lib/network/endpoints"

import _ from "underscore"
import { NotificationManager } from "react-notifications"
import actions from "./names"

// Categories
export const getCategories = () => (dispatch, getState) => {
  dispatch({ type: actions.FETCH_CATEGORIES_BEGIN })

  const { categoriesOptions, categoriesFilters } = getState()
  const data = { ...categoriesOptions, ...categoriesFilters }

  GET({ url: getCategoriesUrl, data }, (err, res) => {
    if (err) {
      return NotificationManager.error(err.message)
    }

    dispatch({
      type: actions.FETCH_CATEGORIES_SUCCESS,
      categories: res.categories,
      total: res.total,
    })
  })
}

// export const getCategorie = (_id) => (dispatch) => {
//   dispatch({ type: actions.FETCH_CATEGORIE_BEGIN })

//   GET({ url: getCategorieUrl, data: { _id } }, (err, categorie) => {
//     if (err) {
//       return NotificationManager.error(err.message)
//     }

//     dispatch({
//       type: actions.FETCH_CATEGORIE_SUCCESS,
//       categorie,
//     })
//   })
// }

export const updateCategoriesOptions = (options) => (dispatch) => {
  dispatch({ type: actions.UPDATE_CATEGORIES_OPTIONS, options })
  dispatch(getCategories())
}

export const updateCategoriesFilters = (filters) => (dispatch) => {
  dispatch({ type: actions.UPDATE_CATEGORIES_OPTIONS, options: { page: 1 } })
  dispatch({ type: actions.UPDATE_CATEGORIES_FILTERS, filters })
  dispatch(getCategories())
}
