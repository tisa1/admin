import initialState from "./initialState"
import actions from "./actions/names"
import _ from "underscore"

// CATEGORIES
export const categories = (state = initialState.categories, action) => {
  switch (action.type) {
  case actions.FETCH_CATEGORIES_SUCCESS:
    return _.clone(action.categories)
  default:
    return state
  }
}

export const categoriesTotal = (state = initialState.categoriesTotal, action) => {
  switch (action.type) {
  case actions.FETCH_CATEGORIES_SUCCESS:
    return action.total
  default:
    return state
  }
}

export const categoriesLoading = (state = initialState.categoriesLoading, action) => {
  switch (action.type) {
  case actions.FETCH_CATEGORIES_BEGIN:
    return true
  case actions.FETCH_CATEGORIES_SUCCESS:
  case actions.FETCH_CATEGORIES_FAILURE:
    return false
  default:
    return state
  }
}

export const categoriesOptions = (state = initialState.categoriesOptions, action) => {
  switch (action.type) {
  case actions.UPDATE_CATEGORIES_OPTIONS:
    return _.extend(state, action.options)
  default:
    return state
  }
}

export const categoriesFilters = (state = initialState.categoriesFilters, action) => {
  switch (action.type) {
  case actions.UPDATE_CATEGORIES_FILTERS:
    return _.extend(state, action.filters)
  default:
    return state
  }
}

// CATEGORIE SINGLE
export const categorie = (state = initialState.categorie, action) => {
  switch (action.type) {
  case actions.FETCH_CATEGORIE_SUCCESS:
    return action.categorie
  default:
    return state
  }
}

export const categorieLoading = (state = initialState.categorieLoading, action) => {
  switch (action.type) {
  case actions.FETCH_CATEGORIE_SUCCESS:
  case actions.FETCH_CATEGORIE_FAILURE:
    return false
  case actions.FETCH_CATEGORIE_BEGIN:
    return true
  default:
    return state
  }
}