import React, { useState } from "react"
import {
  BrowserRouter as Router, Route, Switch
} from "react-router-dom"

import NotFound from "lib/components/NotFound"
import { Layout, Menu } from 'antd';
import {
  UnorderedListOutlined,
  CopyOutlined,
  EditOutlined,
} from '@ant-design/icons';

import Categories from "modules/categories/Categories"
import CatSubcategories from "modules/subcategories/Subcategories"
import Inputs from "modules/inputs/Inputs"
import Templates from "modules/templates/Templates"
import InputAssociations from "modules/input-associations/InputAssociations";

const { Header, Content, Footer, Sider } = Layout;

const App = ()=> {
  const [collapsed, setCollapsed]=useState(false)

  const onCollapse = collapsed => {
    setCollapsed(collapsed )
  };

    return (
      <Layout style={{ minHeight: '100vh' }}>
        <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
          <div className="logo">
            <img 
              style={{width:"60%", margin:"5%"}} 
              src="tisa-black.png"
              alt="tisa-logo"
            />
          </div>
          <Menu theme="dark" defaultSelectedKeys={[window.location.pathname]} mode="inline">
            <Menu.Item onClick={()=>window.location.href="/admin/categorii"} key="/admin/categorii" icon={<UnorderedListOutlined />}>
              Categorii
            </Menu.Item>

            <Menu.Item onClick={()=>window.location.href="/admin/campuri"} key="/admin/campuri" icon={<EditOutlined />}>
              Campuri
            </Menu.Item>
            <Menu.Item onClick={()=>window.location.href="/admin/templateuri"} key="/admin/templateuri" icon={<CopyOutlined />}>
              Template-uri
            </Menu.Item>
            {/* <SubMenu href="/admin/templateuri" key="sub1" icon={<UserOutlined />} title="User">
              <Menu.Item href="/admin/templateuri" key="3">Tom</Menu.Item>
              <Menu.Item href="/admin/categorii/" key="4">Bill</Menu.Item>
              <Menu.Item href="/admin/categorii" key="5">Alex</Menu.Item>
            </SubMenu>
            <SubMenu href="/admin/campuri" key="sub2" icon={<TeamOutlined />} title="Campuri">
              <Menu.Item key="6">Campuri</Menu.Item>
              <Menu.Item key="8">Asocieri</Menu.Item>
            </SubMenu>
            <Menu.Item key="9" icon={<FileOutlined />}>
              Files
            </Menu.Item> */}
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }} />
          <Content style={{ margin: '0 16px' }}>

          <Router>
            <Switch>

              <Route exact path="/admin/categorii" component={Categories} />
              <Route exact path="/admin/categorii/:categoryId" component={CatSubcategories} />
              <Route exact path="/admin/campuri" component={Inputs} />
              <Route exact path="/admin/templateuri" component={Templates} />
              <Route exact path="/admin/templateuri/:templateId" component={InputAssociations} />

              <Route exact component={NotFound} />
            </Switch>
          </Router>
          </Content>
          <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
        </Layout>
      </Layout>

    )
}

     
export default App