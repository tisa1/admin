const api = '/api'

export const SIGN_IN = `${api}/auth/signin/password`
export const SIGN_UP = `${api}/auth/signup/password`

// categories
export const FETCH_CATEGORIES = `${api}/categories/`
export const FETCH_CATEGORY_OPTS = `${api}/categories/options`
export const CREATE_CATEGORY = `${api}/categories/create`
export const DELETE_CATEGORY = `${api}/categories/`

// subcategories
export const FETCH_SUBCATEGORIES_BY_CATEGORY = `${api}/categories/subcategories/by-category`
export const FETCH_SUBCATEGORIES_BY_CATEGORY_OPTS = `${api}/categories/subcategories/by-category/options`
export const CREATE_SUBCATEGORY = `${api}/categories/subcategories/`

// template inputs
export const FETCH_TEMP_INPUTS = `${api}/templates/inputs/`
export const FETCH_TEMP_INPUTS_OPTS = `${api}/templates/inputs/options`
export const CREATE_TEMP_INPUT = `${api}/templates/inputs`
export const DELETE_TEMP_INPUT = `${api}/templates/inputs`

// template input associations
export const FETCH_TEMP_INP_ASSOCIATIONS = `${api}/templates/input-associations`
export const FETCH_TEMP_INP_ASSOCIATIONS_BY_TEMPLATE = `${api}/templates/input-associations/by-template`
export const CREATE_TEMP_INP_ASSOCIATION = `${api}/templates/input-associations`
export const DELETE_TEMP_INP_ASSOCIATION = `${api}/templates/input-associations`

// templates
export const FETCH_TEMPLATES = `${api}/templates/`
export const CREATE_TEMPLATE = `${api}/templates/`
export const DELETE_TEMPLATE = `${api}/templates/`