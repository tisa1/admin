import React, { useEffect, useState, } from "react"
import { 
    List, Avatar, Divider, Button,
    Skeleton, Breadcrumb, Popover,
    Spin,
} from 'antd';
import InputCreate from "./InputCreate"
import notification from "lib/helpers/notification"
import { 
  DELETE_TEMP_INPUT as deleteUrl,
  FETCH_TEMP_INPUTS as fetchUrl,
} from "lib/network/endpoints"
import { DELETE, GET } from "lib/network/requests"
import _ from "underscore"
import { withRouter } from "react-router";

const Categories = ()=>{
    const [ visiblePopover, setVisiblePopover ] = useState(null)
    const [ items, setItems ] = useState([])
    const [ inputsBeingDeleted, setInputsBeingDeleted ] = useState([])
    
    useEffect(()=>{
      getInputs()
    },[])

    const getInputs = ()=>{
      GET({ url:fetchUrl },(err,res)=>{
        if(err){
          return notification.error(err?.message)
        }
        setItems(_.clone(res?.tempInputs))
      })
    }

    const onDeleteInput=(tempInputId)=>{
        setInputsBeingDeleted(_.union(inputsBeingDeleted,[tempInputId]))
        setVisiblePopover(null)
    
        DELETE({ url: deleteUrl, data:{ tempInputId } },(err,res)=>{
          setInputsBeingDeleted(_.without(inputsBeingDeleted,tempInputId))
          if(err){
            return notification.error(err?.message)
          }
          getInputs()
          return notification.success(res?.message)
        })
      }

    const getContent=(inputId)=>{
        return(
          <Button onClick={()=>onDeleteInput(inputId)} danger>Da</Button>
        )
      }

      const handleVisibleChange = inputId => {
        if(inputId === visiblePopover){
          inputId = null
        }
    
        setVisiblePopover(inputId)
      };

      return(
        <>
            <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item href="/admin/campuri">Campuri</Breadcrumb.Item>
            </Breadcrumb>
            <Divider/>
            <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
            
            <InputCreate 
              getInputs={getInputs}
            />
            <Divider/>

            <List
                loading={false}
                itemLayout="horizontal"
                // loadMore={loadMore}
                dataSource={items}
                renderItem={(item,idx) => (
                <List.Item
                    actions={[
                    <Button key="1">Editeaza</Button>, 
                    <Popover 
                        key="2"
                          content={getContent(item?._id)} 
                          visible={visiblePopover === item?._id}
                          onVisibleChange={()=>handleVisibleChange(item?._id)}
                          title="Ești sigur?" 
                          trigger="click"
                        >
                          <Button 
                            disabled={_.includes(inputsBeingDeleted, item?._id)} 
                            type="danger" 
                            key="delete"
                          >
                            {
                              _.includes(inputsBeingDeleted, item?._id)
                              ? <Spin/>
                              : "Șterge"
                            }
                          </Button>
                        </Popover>
                ]}
                >
                    <Skeleton avatar title={false} loading={item?.loading} active>
                    <List.Item.Meta
                        avatar={
                        <Avatar>{idx+1}</Avatar>
                        }
                        title={<a href="https://ant.design">{item?.name}</a>}
                        description="Ant Design, a design language for background applications, is refined by Ant UED Team"
                    />
                    <div>-</div>
                    </Skeleton>
                </List.Item>
                )}
            />
            </div>
        </>
    )
}

export default withRouter(Categories)