import queryString from "query-string"
import store from "redux/store"
import notification from "lib/helpers/notification"
export const POST = async ({ url, data }, clb) => {
  try {
    const { userToken } = store.getState()
    const res = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": userToken ? `Bearer ${userToken}`: null,
      },
      body: JSON.stringify(data),
    })

    const jsonRes = await res.json()
    if (res.status === 403) {

      window.location.href = "/logare"
      // return NotificationManager.error(jsonRes.message || "Session expired")
    }

    return res.ok ? clb(null, jsonRes) : clb(jsonRes, null)
  } catch (error) {
    clb({ message: "Request failed" })
  }
}

export const GET = async ({ url, data }, clb) => {
  try {
    const { userToken } = store.getState()
    const res = await fetch(`${url}?${queryString.stringify(data)}`,{
      headers:{
        "Authorization": userToken ? `Bearer ${userToken}`: null,
      }
    })

    const jsonRes = await res.json()

    if (res.status === 403) {
      if (process.env.NODE_ENV === "development") {
        alert(JSON.stringify(url))
      }

      // session has expired
      delete localStorage.isLoggedIn
      window.location.href = "/logare"
      return notification.error(res?.message);
    }

    return res.ok ? clb(null, jsonRes) : clb(jsonRes, null)
  } catch (error) {
    clb({ message: "Request failed" })
  }
}

export const PUT = async ({ url, data }, clb) => {
  try {
    const { userToken } = store.getState()
    const res = await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        "Authorization": userToken ? `Bearer ${userToken}`: null,
      },
      body: JSON.stringify(data),
    })

    const jsonRes = await res.json()

    if (res.status === 403) {
      if (process.env.NODE_ENV === "development") {
        alert(JSON.stringify(url))
      }

      // session has expired
      delete localStorage.isLoggedIn
      window.location.href = "/logare"
      return notification.error(res?.message || "Session expired");
    }

    return res.ok ? clb(null, jsonRes) : clb(jsonRes, null)
  } catch (error) {
    clb({ message: "Request failed" })
  }
}

export const DELETE = async ({ url, data }, clb) => {
  try {
    const { userToken } = store.getState()
    const res = await fetch(url, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        "Authorization": userToken ? `Bearer ${userToken}`: null,
      },
      body: JSON.stringify(data),
    })

    const jsonRes = await res.json()
    if (res.status === 403) {
      if (process.env.NODE_ENV === "development") {
        alert(JSON.stringify(url))
      }

      // session has expired
      delete localStorage.isLoggedIn
      window.location.href = "/logare"
      return notification.error(res?.message || "Session expired");
    }

    return res.ok ? clb(null, jsonRes) : clb(jsonRes, null)
  } catch (error) {
    clb({ message: "Request failed" })
  }
}

export const PATCH = async ({ url, data }, clb) => {
  try {
    const { userToken } = store.getState()
    const res = await fetch(url, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        "Authorization": userToken ? `Bearer ${userToken}`: null,
      },
      body: JSON.stringify(data),
    })

    const jsonRes = await res.json()

    if (res.status === 403) {
      if (process.env.NODE_ENV === "development") {
        alert(JSON.stringify(url))
      }

      // session has expired
      delete localStorage.isLoggedIn
      window.location.href = "/logare"
      return notification.error(res?.message || "Session expired");
    }

    return res.ok ? clb(null, jsonRes) : clb(jsonRes, null)
  } catch (error) {
    clb({ message: "Request failed" })
  }
}

export default {
  GET,
  POST,
  PUT,
  PATCH,
}
