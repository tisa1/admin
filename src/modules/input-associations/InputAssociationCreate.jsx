import React, { useState, useEffect, useCallback } from 'react'
import { Drawer, Button } from 'antd'
import { Form, Input, Spin, Select } from 'antd'
import { POST, GET } from "lib/network/requests"
import { 
  CREATE_TEMP_INP_ASSOCIATION as createUrl,
  FETCH_TEMP_INPUTS_OPTS as fetchInputOptsUrl,
 } from "lib/network/endpoints"
import notification from "lib/helpers/notification"
import { withRouter } from "react-router"
import _ from "underscore"

const { Option } = Select

const Create= ({ match, getInputAssociations }) => {
  const templateId = match?.params?.templateId
  const [visible, setVisible] = useState(false)
  const [inputOpts, setInputOpts] = useState([])
  const [isCreating, setIsCreating] = useState(false)

  const showDrawer = () => {
    setVisible(true)
  }

  const getInputOpts = useCallback(() => {
    GET({ url: fetchInputOptsUrl },(err,res)=>{
      if(err){
          return notification.error(err?.message)
      }
      return setInputOpts(res.options)
    })
  },[])

  useEffect(() =>{
    getInputOpts()
  },[getInputOpts])

  const closeDrawer = () => {
    setVisible(false)
  }

  const onFinish = (data) => {
    setIsCreating(true)
    _.extend(data,{ templateId })

    POST({ url:createUrl, data },(err,res)=>{
        setIsCreating(false)
        if(err){
            return notification.error(err?.message)
        }
        getInputAssociations()
        setVisible(false)
        return notification.success(res?.message)
    })
  }

  return (
    <>
      <Button shape="circle" type="primary" onClick={showDrawer}>
        +
      </Button>

      <Drawer
        width={500}
        title="Ataseaza camp"
        placement="right"
        closable={false}
        onClose={closeDrawer}
        visible={visible}
      >
       <Form
      name="basic"
      initialValues={{}}
      onFinish={onFinish}
    >
      <Form.Item name="tempInputId" rules={[{ required: true, message: 'Alege campul!' }]}>
        <Select
          placeholder="Camp"
          allowClear
        >
          {
            _.map(inputOpts,(i,idx)=>(
              <Option key={idx} value={i?._id}>{i?.name}</Option>
            ))
          }
        </Select>
      </Form.Item>

      <Form.Item>
        <Button type="primary" block htmlType="submit">
          {
              isCreating
              ? <Spin/>
              : "Ataseaza"
          }
        </Button>
      </Form.Item>
    </Form>
      </Drawer>
    </>
  )
}

export default withRouter(Create)