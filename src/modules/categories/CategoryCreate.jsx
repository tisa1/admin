import React, { useState } from 'react'
import { Drawer, Button } from 'antd'
import { Form, Input, Spin } from 'antd'
import { POST } from "lib/network/requests"
import { CREATE_CATEGORY as createCategoryUrl } from "lib/network/endpoints"
import notification from "lib/helpers/notification"
import { useDispatch } from "react-redux";
import { getCategories } from "./actions/categories"

const App= () => {
  const dispatch = useDispatch()
  const [visible, setVisible] = useState(false)
  const [isCreating, setIsCreating] = useState(false)

  const showDrawer = () => {
    setVisible(true);
  }

  const closeDrawer = () => {
    setVisible(false)
  }

  const onFinish = (data) => {
    setIsCreating(true)
    POST({url:createCategoryUrl, data},(err,res)=>{
        setIsCreating(false)
        if(err){
            return notification.error(err?.message)
        }
        setVisible(false)
        dispatch(getCategories())
        return notification.success(res?.message)
    })
  }

  const onFinishFailed = (errorInfo) => {
  }

  return (
    <>
      <Button shape="circle" type="primary" onClick={showDrawer}>
        +
      </Button>

      <Drawer
        width={500}
        title="Creaza categorie"
        placement="right"
        closable={false}
        onClose={closeDrawer}
        visible={visible}
      >
       <Form
      name="basic"
      initialValues={{}}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Form.Item
        name="name"
        rules={[{ required: true, message: 'Completeaza nume!' }]}
      >
        <Input
            placeholder="Nume"
        />
      </Form.Item>

      <Form.Item>
        <Button type="primary" block htmlType="submit">
          {
              isCreating
              ? <Spin/>
              : "Creaza"
          }
        </Button>
      </Form.Item>
    </Form>
      </Drawer>
    </>
  );
};

export default App