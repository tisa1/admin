import React, { useState, useEffect, useCallback } from 'react'
import { Drawer, Button } from 'antd'
import { Form, Spin, Select } from 'antd'
import { POST, GET } from "lib/network/requests"
import { 
  CREATE_TEMPLATE as createUrl,
  FETCH_CATEGORY_OPTS as fetchCatOptsUrl,
  FETCH_SUBCATEGORIES_BY_CATEGORY_OPTS as fetchSubcatOptsUrl,
 } from "lib/network/endpoints"
import notification from "lib/helpers/notification"
import { withRouter } from "react-router"
import _ from "underscore"

const { Option } = Select

const Create= ({ getTemplates}) => {

  const [visible, setVisible] = useState(false)
  const [categoryOpts, setCategoryOpts] = useState([])
  const [categoryId, setCategoryId] = useState(null)
  const [subcatOpts, setSubcatOpts] = useState([])
  const [isCreating, setIsCreating] = useState(false)

  const showDrawer = () => {
    setVisible(true)
  }

  const getCategoryOpts=useCallback(()=>{
    GET({ url:fetchCatOptsUrl },(err,res)=>{
      if(err){
          return notification.error(err?.message)
      }
      setCategoryOpts(res.options)
    })
  },[])

  const getSubcategoryOpts = useCallback(() => {
    GET({ url:fetchSubcatOptsUrl, data:{ categoryId } },(err,res)=>{
      if(err){
          return notification.error(err?.message)
      }
      setSubcatOpts(res.options)
    })
  },[categoryId])

  useEffect(() =>{
    getCategoryOpts()
  },[getCategoryOpts])

  useEffect(() =>{
    if(!categoryId) return
    getSubcategoryOpts()
  },[categoryId, getSubcategoryOpts])

  const closeDrawer = () => {
    setVisible(false)
  }

  const onFinish = (data) => {
    setIsCreating(true)
    POST({ url:createUrl, data },(err,res)=>{
        setIsCreating(false)
        if(err){
            return notification.error(err?.message)
        }
        getTemplates()
        setVisible(false)
        return notification.success(res?.message)
    })
  }

  const onCategorySelect=(categoryId)=>{
    setCategoryId(categoryId)
  }

  return (
    <>
      <Button shape="circle" type="primary" onClick={showDrawer}>
        +
      </Button>

      <Drawer
        width={500}
        title="Creaza template"
        placement="right"
        closable={false}
        onClose={closeDrawer}
        visible={visible}
      >
       <Form
      name="basic"
      initialValues={{}}
      onFinish={onFinish}
    >
      <Form.Item name="category" rules={[{ required: true, message: 'Alege categoria!'  }]}>
        <Select
          onChange={onCategorySelect}
          placeholder="Categoria"
          allowClear
        >
          {
            _.map(categoryOpts,i=>(
              <Option value={i?._id}>{i?.name}</Option>

            ))
          }
        </Select>
      </Form.Item>

      {
        categoryId
        && <Form.Item name="subcategoryId" rules={[{ required: true, message: 'Alege subcategoria!'  }]}>
          <Select
            placeholder="Subcategoria"
            allowClear
          >
            {
              _.map(subcatOpts,i=>(
                <Option value={i?._id}>{i?.name}</Option>
              ))
            }
          </Select>
        </Form.Item>
      }


      <Form.Item>
        <Button type="primary" block htmlType="submit">
          {
              isCreating
              ? <Spin/>
              : "Creaza"
          }
        </Button>
      </Form.Item>
    </Form>
      </Drawer>
    </>
  )
}

export default withRouter(Create)