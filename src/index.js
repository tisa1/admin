
import React from "react"
import ReactDOM from "react-dom"
import App from "redux/containers/App"
import { NotificationContainer } from "react-notifications"
import { Provider } from "react-redux"
import store from "redux/store"
import "react-notifications/lib/notifications.css"
import 'antd/dist/antd.css'; 

ReactDOM.render(
    <Provider store={store}>
        <App />
        <NotificationContainer />
    </Provider>,
    document.getElementById("root"))
