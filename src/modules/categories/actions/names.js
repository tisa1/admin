const names = {
  // Categories
  FETCH_CATEGORIES_BEGIN: "Fetch Categories Begin",
  FETCH_CATEGORIES_FAILURE: "Fetch Categories Failure",
  FETCH_CATEGORIES_SUCCESS: "Fetch Categories Success",

  FETCH_TOP_CATEGORIES_BEGIN: "Fetch Top Categories Begin",
  FETCH_TOP_CATEGORIES_FAILURE: "Fetch Top Categories Failure",
  FETCH_TOP_CATEGORIES_SUCCESS: "Fetch Top Categories Success",

  FETCH_CATEGORIE_BEGIN: "Fetch Categorie Begin",
  FETCH_CATEGORIE_FAILURE: "Fetch Categorie Failure",
  FETCH_CATEGORIE_SUCCESS: "Fetch Categorie Success",

  UPDATE_CATEGORIES_OPTIONS: "Update Categories Options",
  UPDATE_CATEGORIES_FILTERS: "Update Categories Filters",
}

export default names