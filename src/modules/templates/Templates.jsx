import React, { useEffect, useState, } from "react"
import { 
    List, Avatar, Divider, Button,
    Skeleton, Breadcrumb, Popover,
    Spin,
} from 'antd';
import TemplateCreate from "./TemplateCreate"
import notification from "lib/helpers/notification"
import { 
  DELETE_TEMPLATE as deleteUrl,
  FETCH_TEMPLATES as fetchUrl,
} from "lib/network/endpoints"
import { DELETE, GET } from "lib/network/requests"
import _ from "underscore"
import { withRouter } from "react-router";

const Categories = ()=>{
    const [ visiblePopover, setVisiblePopover ] = useState(null)
    const [ items, setItems ] = useState([])
    const [ inputsBeingDeleted, setTemplatesBeingDeleted ] = useState([])
    
    useEffect(()=>{
      getTemplates()
    },[])

    const getTemplates = ()=>{
      GET({ url:fetchUrl },(err,res)=>{
        if(err){
          return notification.error(err?.message)
        }
        setItems(_.clone(res?.templates))
      })
    }

    const onDeleteTemplate=(templateId)=>{
        setTemplatesBeingDeleted(_.union(inputsBeingDeleted,[templateId]))
        setVisiblePopover(null)
    
        DELETE({ url: deleteUrl, data:{ templateId } },(err,res)=>{
          setTemplatesBeingDeleted(_.without(inputsBeingDeleted, templateId))
          if(err){
            return notification.error(err?.message)
          }
          getTemplates()
          return notification.success(res?.message)
        })
      }

    const getContent=(inputId)=>{
        return(
          <Button onClick={()=>onDeleteTemplate(inputId)} danger>Da</Button>
        )
      }

      const handleVisibleChange = inputId => {
        if(inputId === visiblePopover){
          inputId = null
        }
    
        setVisiblePopover(inputId)
      };

      return(
        <>
            <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item href="/admin/templateuri">Template-uri</Breadcrumb.Item>
            </Breadcrumb>
            <Divider/>
            <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
            
            <TemplateCreate 
              getTemplates={getTemplates}
            />
            <Divider/>

            <List
                loading={false}
                itemLayout="horizontal"
                dataSource={items}
                renderItem={(item,idx) => (
                <List.Item
                    actions={[
                    <Button key="1">Editeaza</Button>, 
                    <Popover 
                        key="2"
                          content={getContent(item?._id)} 
                          visible={visiblePopover === item?._id}
                          onVisibleChange={()=>handleVisibleChange(item?._id)}
                          title="Ești sigur?" 
                          trigger="click"
                        >
                          <Button 
                            disabled={_.includes(inputsBeingDeleted, item?._id)} 
                            type="danger" 
                            key="delete"
                          >
                            {
                              _.includes(inputsBeingDeleted, item?._id)
                              ? <Spin/>
                              : "Șterge"
                            }
                          </Button>
                        </Popover>
                ]}
                >
                    <Skeleton avatar title={false} loading={item?.loading} active>
                    <List.Item.Meta
                        avatar={
                        <Avatar>{idx+1}</Avatar>
                        }
                        title={<a href={`/admin/templateuri/${item?._id}`}>{item?.category?.name}</a>}
                        description={item?.subcategory?.name}
                    />

                    <div>-</div>
                    </Skeleton>
                </List.Item>
                )}
            />
            </div>
        </>
    )
}

export default withRouter(Categories)